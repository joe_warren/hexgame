# Hex Game (Working title)

This is a work in progress 3d roguelike. 

TBH, I don't hold out any hope for this reaching completion; It's more of a learning exercise. 

## Running

Assuming you have [Stack](https://docs.haskellstack.org/en/stable/README/) installed:

    stack run

## To Do

See [TODO.md](TODO.MD)
