{-# LANGUAGE TemplateHaskell #-}
module Model
( ModelType (..)
, Counter (..)
, StationaryCounter (..)
, MovingCounter (..)
, counterModelType
, CursorState (..)
, modelFileName
, gameMapToCounters
) where

import qualified GameMap as GM
import Data.Countable as DC
import Data.Searchable as DS
import qualified Data.Map as M
import Control.Lens

data CursorState = CursorStart | CursorEnd | CursorArrow | CursorArrowDown | CursorArrowUp | CursorTimer deriving (Eq, Ord, Enum, Bounded, Show)

data ModelType = PlayerModel | BarrelModel | SpiderModel | StaircaseDownModel | StaircaseUpModel | CursorModel CursorState | TileModel GM.Tile deriving (Eq, Ord, Show)

instance DS.Searchable ModelType where
  search = finiteSearch

instance DC.Countable ModelType where
  countPrevious = finiteCountPrevious
  countMaybeNext = finiteCountMaybeNext

getAllValues :: (Bounded a, Enum a) => (a -> b) -> [b]
getAllValues f = f <$> [minBound ..]

instance DS.Finite ModelType where
  allValues = PlayerModel : BarrelModel : SpiderModel : StaircaseDownModel : StaircaseUpModel : (getAllValues CursorModel) ++ (getAllValues TileModel)

modelFileName :: ModelType -> String
modelFileName PlayerModel = "wizard/wizard.json"
modelFileName SpiderModel = "creatures/spider/spider.json"
modelFileName BarrelModel = "objects/barrel/barrel.json"
modelFileName StaircaseDownModel = "objects/staircase_down/staircase_down.json"
modelFileName StaircaseUpModel = "objects/staircase_up/staircase_up.json"
modelFileName (CursorModel CursorStart) = "cursor/start/cursor.json"
modelFileName (CursorModel CursorEnd) = "cursor/end/cursor.json"
modelFileName (CursorModel CursorArrow) = "cursor/arrow/cursor.json"
modelFileName (CursorModel CursorArrowDown) = "cursor/down/cursor.json"
modelFileName (CursorModel CursorArrowUp) = "cursor/up/cursor.json"
modelFileName (CursorModel CursorTimer) = "cursor/timer/cursor.json"
modelFileName (TileModel GM.Grass) = "tiles/grass/grass.json"
modelFileName (TileModel GM.Water) = "tiles/water/water.json"
modelFileName (TileModel GM.Path) = "tiles/stone/stone.json"
modelFileName (TileModel GM.Hole) = "tiles/hole/hole.json"
modelFileName (TileModel GM.FullHole) = "tiles/full_hole/full_hole.json"

data Counter = Moving MovingCounter | Stationary StationaryCounter

data MovingCounter = MovingCounter {
   movingCounterSrc :: (Int, Int),
   movingCounterDst :: (Int, Int),
   movingCounterSrcAngle :: Int, 
   movingCounterDstAngle :: Int, 
   movingCounterModelType :: ModelType
}

data StationaryCounter = StationaryCounter {
   stationaryCounterPosition :: (Int, Int),
   stationaryCounterAngle :: Int,
   stationaryCounterModelType :: ModelType
}

counterModelType ::Counter -> ModelType
counterModelType (Moving m) = movingCounterModelType m
counterModelType (Stationary s) = stationaryCounterModelType s

tileToCounter :: ((Int, Int), GM.Tile) -> Counter
tileToCounter (pos, tile) = Stationary (StationaryCounter pos 0 (TileModel tile))

gameMapToCounters :: GM.GameMap -> [Model.Counter]
gameMapToCounters = (fmap tileToCounter) . M.toAscList . (^.GM.tiles)

