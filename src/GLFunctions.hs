module GLFunctions 
( ProgramLocations (..)
, bufferOffset 
, bindMatrix
, readDepth
, projectionMatrix
, backProject
, extendV3
, normalizeV4
) where 

import Foreign.Ptr
import Graphics.UI.GLUT
import Foreign.Storable (peek)
import Foreign.Marshal.Utils (with)
import qualified Linear as L

bufferOffset :: Integral a => a -> Ptr b
bufferOffset = plusPtr nullPtr . fromIntegral

data ProgramLocations = ProgramLocations {
   programMatrixUniform :: UniformLocation, 
   programPositionAttrib :: AttribLocation, 
   programUVAttrib :: AttribLocation
}

toGLMat :: (L.V4 (L.V4 GLfloat)) -> [GLfloat] 
toGLMat (L.V4 (L.V4 x1 y1 z1 w1) (L.V4 x2 y2 z2 w2) (L.V4 x3 y3 z3 w3) (L.V4 x4 y4 z4 w4)) = [x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4, w1, w2, w3, w4]

bindMatrix :: ProgramLocations -> L.M44 Float -> IO ()
bindMatrix programLocs m = do
  transform <- newMatrix ColumnMajor (toGLMat m) :: IO (GLmatrix GLfloat)
  uniform (programMatrixUniform programLocs) $= transform

readDepth :: Int -> Int -> IO Float
readDepth x y = do
  let p = Position (fromIntegral x)  (fromIntegral y)
  let s = Size 1 1
  let f = 0 :: Float
  with f $ \ptr -> do
    let pd = PixelData DepthComponent Float ptr
    readPixels p s pd 
    peek ptr

sizedProjectionMatrix :: Float -> Float -> L.M44 Float
sizedProjectionMatrix width height = perspectiveMat L.!*! directionMat
  where
    directionMat = L.lookAt (L.V3 0 20 20) (L.V3 0 0 2) (L.V3 0 0 1)
    aspect = width/height
    perspectiveMat = L.perspective 1 aspect 1 100

projectionMatrix :: IO (L.M44 Float)
projectionMatrix = do
  Size width height <- get windowSize
  return $ sizedProjectionMatrix (fromIntegral width) (fromIntegral height)

backProject :: Int -> Int -> IO (Maybe (L.V4 Float))
backProject x y = do
  Size windowWidth windowHeight <- get windowSize
  let width = fromIntegral windowWidth :: Int
  let height = fromIntegral windowHeight :: Int
  d <- readDepth (fromIntegral x) (fromIntegral (height - y))
  let v = L.V4 ((2*(fromIntegral x)/(fromIntegral width))-1) (1-2*((fromIntegral y)/(fromIntegral height))) (2*d-1) 1 :: L.V4 Float
  projection <- projectionMatrix
  let p = (L.inv44 projection) L.!* v
  return $ if d == 1.0 then Nothing else Just p


extendV3 :: L.V3 Float -> L.V4 Float
extendV3 (L.V3 x y z) = L.V4 x y z 1

normalizeV4 :: L.V4 Float -> L.V3 Float
normalizeV4 (L.V4 x y z w)  = L.V3 (x/w) (y/w) (z/w)
