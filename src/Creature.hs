{-# LANGUAGE TemplateHaskell #-}
module Creature 
( Species (..)
, Creature (..)
, Exit (..)
, species 
, creaturePosition 
, creatureDirection 
, creatureModel
, creatureShape
, occupiedTiles
, Behaviour (..)
, Action (..)
, Transition (..)
, isPushable 
, getExit
, toCounter
, transitionToCounter
) where 

import qualified Model
import qualified GameMap as GM
import Control.Lens
import qualified Math.Geometry.Grid.HexagonalInternal as HGI

data Species = Player | Monster Action Behaviour

data Exit = ExitUpwards | ExitDownwards Int deriving Show
data Action = NoAction | Pushable | ExitAction Exit
data Behaviour = NoBehaviour | RandomWalk

data Creature = Creature { 
  _species :: Species, 
  _creaturePosition :: (Int, Int),
  _creatureDirection :: Int,
  _creatureShape :: [[HGI.HexDirection]],
  _creatureModel :: Model.ModelType
}

makeLenses ''Creature

occupiedTiles :: Creature -> [(Int, Int)]
occupiedTiles c = GM.constructShape (c^.creatureShape) (c^.creaturePosition) 

data Transition = MoveTransition (Int, Int) | PushTransition (Int, Int) | NullTransition | ExitTransition Exit (Int, Int) deriving Show

toCounter :: Creature -> Model.Counter
toCounter c = Model.Stationary $ Model.StationaryCounter (c^.creaturePosition) (c^.creatureDirection) (c^.creatureModel)

transitionToCounter :: Transition -> Creature -> Model.Counter
transitionToCounter (MoveTransition dst) c = movingTransitionTo dst c
transitionToCounter (PushTransition dst) c = movingTransitionTo dst c
transitionToCounter NullTransition c = toCounter c
transitionToCounter (ExitTransition _ dst) c = movingTransitionTo dst c


movingTransitionTo :: (Int, Int) -> Creature -> Model.Counter
movingTransitionTo dst c = 
  Model.Moving $ Model.MovingCounter 
    (c^.creaturePosition) dst
    (c^.creatureDirection) (GM.angleBetween (c^.creaturePosition) dst)
    (c^.creatureModel)

isPushable :: Creature -> Bool
isPushable Creature{_species=Monster Pushable _} = True
isPushable _ = False

getExit :: Creature -> Maybe Exit
getExit Creature{_species=Monster (ExitAction e) _} = Just e
getExit _ = Nothing
