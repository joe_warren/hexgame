module MinSpanTree (
   minSpanTree,
   Edge(..)
) where

import Data.List(sort)
import Data.Set(member, insert, singleton, Set)

data Edge a = Edge a a Double deriving (Eq, Show)

instance Ord a => Ord (Edge a) where
   compare (Edge a b c) (Edge d e f) = 
      (c, min a b, max a b) `compare` (f, min d e, max d e)

minSpanTree :: (Show a, Ord a) => [Edge a] -> [Edge a]
minSpanTree [] = []
minSpanTree (edges@((Edge node _ _):_)) = loop (sort edges) [] startSet
  where
    startSet = singleton node

loop :: (Show b, Ord b) =>
              [Edge b] -> [Edge b] -> Set b -> [Edge b]
loop [] solution _ = solution
loop edges solution vertices = 
   let (e,x) = findNextEdge edges vertices
       vertices' = x `insert` vertices
       cyclicEdge (Edge a b _) = a `member` vertices' && 
                                 b `member` vertices' 
       edges' = filter (not.cyclicEdge) edges 
   in loop edges' (e:solution) vertices'   

findNextEdge :: (Show a, Ord a) => [Edge a] -> Set a -> (Edge a, a)
findNextEdge [] vs = error ("Disjunct graph with island " ++ show vs)
findNextEdge (e@(Edge a b _):edges) vertices 
    | a `member` vertices = (e,b)
    | b `member` vertices = (e,a)
    | otherwise = findNextEdge edges vertices
