module RenderModel 
( ModelDescriptor (..)
, initModelDescriptors
, renderModelDescriptor
, RenderContext (..)
, renderCounter
) where

import Graphics.UI.GLUT

import Control.Monad
import Foreign.Marshal.Array
import Foreign.Storable
import Data.Fixed (mod')
import qualified Linear as L
import qualified JuicyUtils as JU
import qualified Data.Vector as V
import qualified Data.Text as Text
import qualified Codec.Wavefront.IO as OBJIO
import qualified Codec.Wavefront as OBJ
import qualified GLFunctions as GLF
import qualified ModelDocument as MD
import qualified Model
import Data.Searchable (assemble)

data ModelDescriptor = ModelDescriptor {
  tree :: ModelDescriptorNode, 
  speed :: Float,
  textureObject :: TextureObject
}

data ModelDescriptorNode = ModelDescriptorNode {
  nodeVAO :: VertexArrayObject,
  nodeBO ::  BufferObject,
  nodeVertexCount :: NumArrayIndices,
  nodePivot :: L.V3 Float,
  nodeFrames :: [ModelDescriptorFrame],
  nodeChildren :: [ModelDescriptorNode]
}

data ModelDescriptorFrame = ModelDescriptorFrame{
  frameTime :: Float, 
  frameQuaternion :: L.Quaternion Float
}

objFaceToTris :: V.Vector (OBJ.Element OBJ.Face) -> V.Vector OBJ.Location -> [[Float]]
objFaceToTris faces locs = concat $ V.toList vVecs
  where
    rawFaces = OBJ.elValue <$> faces
    lup = (flip (V.!)) . (max 0) . ((+) (-1)) . OBJ.faceLocIndex 
    aLocs = (\(OBJ.Face i1 i2 i3 _) -> [lup i1, lup i2, lup i3] <*> pure locs) <$> rawFaces
    vVecs = (fmap (\(OBJ.Location x y z _) -> [x, y, z])) <$> aLocs
    
objFaceToUVs :: V.Vector (OBJ.Element OBJ.Face) -> V.Vector OBJ.TexCoord -> [[Float]]
objFaceToUVs faces coords = concat $ V.toList vVecs
  where
    rawFaces = OBJ.elValue <$> faces
    lup = (\(Just l) -> (V.! (max 0 (l-1)))) . OBJ.faceTexCoordIndex 
    aLocs = (\(OBJ.Face i1 i2 i3 _) -> [lup i1, lup i2, lup i3] <*> pure coords) <$> rawFaces
    vVecs = (fmap (\(OBJ.TexCoord r s _) -> [r, -s])) <$> aLocs
 
filterFace :: String -> OBJ.Element OBJ.Face -> Bool
filterFace name el = (Text.unpack <$> OBJ.elObject el) == (Just name)

documentFrameToDescriptor :: MD.ModelFrame -> ModelDescriptorFrame
documentFrameToDescriptor mf = ModelDescriptorFrame t quat
  where 
    [w, x, y, z] = MD.d mf
    t = MD.t mf
    quat = L.Quaternion (w) (L.V3 (x) (y) (z))

documentNodeToDescriptor :: OBJ.WavefrontOBJ -> MD.ModelTree -> IO ModelDescriptorNode
documentNodeToDescriptor obj modelTree = do
  let name = MD.name modelTree
  let [x, y, z] = MD.head modelTree
  let pivot = L.V3 (x) (y) (z)
  let frames = fmap documentFrameToDescriptor (MD.frames modelTree)
  let faces = V.filter (filterFace name) $ OBJ.objFaces obj
  let locations = OBJ.objLocations obj
  let coords = OBJ.objTexCoords obj
  let tris = objFaceToTris faces locations
  let uvs = objFaceToUVs faces coords
  let vertices = concat $ (uncurry (++)) <$> zip tris uvs
    
  vao <- genObjectName
  bindVertexArrayObject $= Just vao

  arrayBuffer <- genObjectName
  bindBuffer ArrayBuffer $= Just arrayBuffer

  let numVertices = (length vertices) 
  let vertexSize = fromIntegral (sizeOf (head vertices))

  withArray vertices $ \ptr -> do
    let size = fromIntegral (numVertices * vertexSize)

    bufferData ArrayBuffer $= (size, ptr, StaticDraw)

  children <- sequence $ documentNodeToDescriptor obj <$> (MD.children modelTree)
  return $ ModelDescriptorNode vao arrayBuffer (fromIntegral numVertices) pivot frames children
 
initModelDescriptors :: IO (Model.ModelType -> ModelDescriptor)
initModelDescriptors = assemble (initModelDescriptor . Model.modelFileName)

initModelDescriptor :: String -> IO ModelDescriptor
initModelDescriptor filename = do
  eitherDocument <- MD.readDocument filename
  document <- case eitherDocument of
                Left e -> error e
                Right d -> return d  
  putStrLn $ MD.describeDocStructure document

  Right obj <- OBJIO.fromFile (MD.object document)

  tex <- JU.loadImage $ MD.texture document
  textureObjectLODBias Texture2D $= -1
  generateMipmap' Texture2D 
  textureFilter Texture2D $= ((Linear', Just Linear'), Linear')
  --texture2DWrap $= (Repeated, ClampToEdge)
  texture Texture2D        $= Enabled

  modelTree <- documentNodeToDescriptor obj (MD.tree document)
  return $ ModelDescriptor modelTree (MD.speed document) tex

frameBounds :: Float -> [ModelDescriptorFrame] -> (ModelDescriptorFrame, ModelDescriptorFrame)
frameBounds _ [] = error "look up of empty frame bounds"
frameBounds _ (_:[]) = error "look up of frame bounds on a single frame"
frameBounds time (a:b:xs) = if inBounds then (a, b) else (frameBounds time (b:xs))
  where 
    inBounds = ((frameTime a) <= time) && ((frameTime b) >= time)

quaternionAtTime :: Float -> [ModelDescriptorFrame] -> L.Quaternion Float
quaternionAtTime _ [] = L.axisAngle (L.V3 1 0 0) 0
quaternionAtTime _ (f:[]) = frameQuaternion f
quaternionAtTime time frames = L.slerp (frameQuaternion low) (frameQuaternion high) (elapsed/range)
  where
   bound = maximum $ frameTime <$> frames
   clamped = time `mod'` bound
   (low, high) = frameBounds clamped frames
   range = (frameTime high) - (frameTime low)
   elapsed = clamped - (frameTime low) 
   

renderModelDescriptorNode :: GLF.ProgramLocations -> L.M44 Float -> Float -> ModelDescriptorNode -> IO()
renderModelDescriptorNode programLocs mat time node = do
  bindVertexArrayObject $= Just (nodeVAO node)
  bindBuffer ArrayBuffer $= Just (nodeBO node)

  let vertexSize = sizeOf (undefined::GLfloat)
  let stride = fromIntegral (5 * vertexSize)
  let count = nodeVertexCount node
  let posAttrib = GLF.programPositionAttrib programLocs
  vertexAttribPointer posAttrib $=
    (ToFloat,
     VertexArrayDescriptor 3 Float stride (GLF.bufferOffset (0::Integer)))
  let pivot = nodePivot node
  let q = quaternionAtTime time $ nodeFrames node

  let thisMatrix = (L.mkTransformation q (pivot)) L.!*! (L.mkTransformationMat L.identity (-pivot))
  let m = mat L.!*! thisMatrix
  GLF.bindMatrix programLocs m

  vertexAttribArray posAttrib $= Enabled
  let uvAttrib = GLF.programUVAttrib programLocs
  vertexAttribPointer uvAttrib $=
    (ToFloat,
     VertexArrayDescriptor 2 Float stride (GLF.bufferOffset (3 * vertexSize)))
  vertexAttribArray uvAttrib $= Enabled

  drawArrays Triangles 0 count
  void $ sequence $ renderModelDescriptorNode programLocs m time <$> (nodeChildren node)

renderModelDescriptor :: GLF.ProgramLocations -> L.M44 Float -> Float -> ModelDescriptor -> IO ()
renderModelDescriptor programLocs mat time modelDescriptor = do
  textureBinding Texture2D $= Just (textureObject modelDescriptor)
  renderModelDescriptorNode programLocs mat (time * (speed modelDescriptor)) (tree modelDescriptor)

data RenderContext = RenderContext {
  contextProgramLocs :: GLF.ProgramLocations,
  ctxModelLookup :: (Model.ModelType -> ModelDescriptor)
}

cartesianFromHex :: (Int, Int) -> L.V2 Float
cartesianFromHex (x, y) = L.V2 xc yc
  where 
    x1 = fromIntegral x
    y1 = fromIntegral y
    xc = 1.5*x1
    yc = (2*y1 + x1) * cos (pi/6)

counterPosition :: Model.Counter -> Float -> L.V2 Float
counterPosition (Model.Stationary c) _ = cartesianFromHex $ Model.stationaryCounterPosition c
counterPosition (Model.Moving c) t = L.lerp t end start
  where
    start = cartesianFromHex $ Model.movingCounterSrc c
    end = cartesianFromHex $ Model.movingCounterDst c

directionToAngle :: Int -> Float
directionToAngle d =  pi * 2 * (fromIntegral $ d) /6

counterAngle :: Model.Counter -> Float -> Float
counterAngle (Model.Stationary c)  _ = directionToAngle $ Model.stationaryCounterAngle c
counterAngle (Model.Moving c) t = res
  where
    amountThroughArc = min 1.0 (t*10)
    start = directionToAngle $ Model.movingCounterSrcAngle c
    end = directionToAngle $ Model.movingCounterDstAngle c
    L.V1 res = L.lerp amountThroughArc (L.V1 end) (L.V1 start)

renderCounter :: RenderContext -> L.M44 Float -> Float -> Model.Counter -> IO ()
renderCounter ctx mat time counter = renderModelDescriptor locs m time modelDescriptor 
  where 
    locs = contextProgramLocs ctx
    modelDescriptor = (ctxModelLookup ctx) $ Model.counterModelType counter
    L.V2 x y = counterPosition counter time
    a = counterAngle counter time
    modelMat = L.mkTransformation (L.axisAngle (L.V3 0 0 1) a) (L.V3 x y 0)
    m = mat L.!*! modelMat
    
