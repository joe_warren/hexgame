{-# LANGUAGE DeriveGeneric #-}
module ModelDocument 
(  ModelDocument (..), 
   ModelTree (..), 
   ModelFrame (..),
   readDocument, 
   describeDocStructure
) where

import GHC.Generics
import Data.Aeson
import Data.List

data ModelDocument = ModelDocument {
  object :: String, 
  texture :: String,
  speed :: Float,
  tree :: ModelTree
} deriving (Generic, Show)

data ModelTree = ModelTree {
  name :: String,
  head :: [Float],
  tail :: [Float],
  frames :: [ModelFrame],
  children :: [ModelTree]
} deriving (Generic, Show)

data ModelFrame = ModelFrame {
  t :: Float,
  d :: [Float]
} deriving (Generic, Show)

instance FromJSON ModelDocument
instance FromJSON ModelTree
instance FromJSON ModelFrame

readDocument :: String -> IO (Either String ModelDocument)
readDocument = eitherDecodeFileStrict

describeTreeStructure :: Int -> ModelTree -> String
describeTreeStructure depth tr = intercalate "\n" $ thisLine:childLines
  where
    thisLine = (replicate depth ' ') ++ (name tr)
    childLines = describeTreeStructure (depth+1) <$> children tr

describeDocStructure :: ModelDocument -> String
describeDocStructure doc = describeTreeStructure 0 $ tree doc
