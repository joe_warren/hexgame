module Trackball 
( Trackball(..)
, trackballMatrix
, newTrackball
, click
, unclick
, scrollUp
, scrollDown
, drag
) where

import qualified Linear as L
import qualified GLFunctions as GLF
data Trackball = Trackball {
  clicked :: Bool,
  lastPosition :: Maybe (L.V3 Float),
  currentAngle :: Float,
  currentScale :: Float,
  currentTranslation :: (Float, Float)
}

trackballMatrix :: Trackball -> L.M44 Float
trackballMatrix t = scale L.!*! rotation L.!*! translation
 where
   scale = L.m33_to_m44 (L.identity L.!!* (currentScale t))
   rotation = (L.m33_to_m44 . L.fromQuaternion . trackballQuaternion) t
   (x, y) = currentTranslation t
   translation = L.mkTransformationMat L.identity (L.V3 (-x) (-y) 0)

trackballQuaternion :: Trackball -> L.Quaternion Float
trackballQuaternion Trackball{currentAngle=a} = L.axisAngle (L.V3 0 0 1) a

click :: Trackball -> Trackball
click t = t {clicked = True}

unclick :: Trackball -> Trackball 
unclick t = t {clicked = False, lastPosition = Nothing}

scrollUp :: Trackball -> Trackball 
scrollUp t = t {currentScale = 0.95 * (currentScale t)}

scrollDown :: Trackball -> Trackball 
scrollDown t = t {currentScale = (currentScale t)/0.95}

newTrackball :: Trackball
newTrackball = Trackball False (Just $ L.V3 0 0 0) 0 1 (0, 0)

flattern :: L.V3 Float -> L.V2 Float
flattern (L.V3 x y _) = L.V2 x y 

angleDirection :: L.V2 Float -> L.V2 Float -> Float 
angleDirection a b = if sign == 0 then 1 else sign
  where 
    sign = (signum $ L.crossZ a b)

withUpdatedClick :: (L.V3 Float) -> (Trackball -> Trackball) -> Trackball -> Trackball
withUpdatedClick clickAt fn oldTB = newTB {lastPosition = Just newClick}
  where
    newTB = fn oldTB
    rawClick = (trackballMatrix oldTB) L.!* (GLF.extendV3 clickAt)
    newClick = GLF.normalizeV4 $ (L.inv44 $ trackballMatrix newTB) L.!* rawClick


rotateTB :: L.V3 Float -> Trackball -> Trackball 
rotateTB clickAt t@Trackball{lastPosition=(Just lp)} = t {currentAngle=newAngle}
  where
    origin = (uncurry L.V2) $ currentTranslation t
    lastPositionFlat = (flattern lp) L.^-^ origin
    clickFlat = (flattern clickAt) L.^-^ origin
    dotProd = ((L.normalize clickFlat) `L.dot` (L.normalize $ lastPositionFlat))
    ang = (acos dotProd) * (angleDirection lastPositionFlat clickFlat)
    newAngle = if isNaN ang then (currentAngle t) else (currentAngle t) + ang
rotateTB _ t = t 

translateTB :: L.V3 Float -> Trackball -> Trackball
translateTB clickAt t@Trackball{lastPosition=(Just lp)} = t {currentTranslation=newPosition}
  where
    lastPositionFlat = flattern lp
    clickFlat = flattern clickAt
    dv = (clickFlat L.^-^ lastPositionFlat)
    L.V2 dx dy = dv 
    (x, y) = currentTranslation t
    newPosition = (x - dx, y - dy)
translateTB _ t = t 

drag :: Bool -> L.V3 Float -> Trackball -> Trackball
drag _ clickAt t@Trackball{clicked=False} = ((withUpdatedClick clickAt) id) t
drag False clickAt t = ((withUpdatedClick clickAt) (rotateTB clickAt)) t
drag True clickAt t = ((withUpdatedClick clickAt) (translateTB clickAt)) t
