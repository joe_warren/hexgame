{-# LANGUAGE TemplateHaskell #-}
module Board 
( Board (..)
, boardMap 
, boardPlayer 
, boardCreatures 
, intendedPlayerTransitions 
, boardRandomGen 
, fromMapRandomizingStart
, addCreaturesRandomly
, addCreatureRemovingTiles
, toCounters
, isInTransition
, advance
, cancelTransitions
, routeTo
, Route (..)
, moveTo
) where

import Control.Lens
import Data.List (minimumBy)
import Data.Maybe (catMaybes, fromJust, isJust)
import Data.Foldable (find)
import Control.Monad (join)
import Data.Function (on)

import qualified Data.Map as M
import qualified Math.Geometry.Grid as G
import qualified GameMap as GM
import qualified Creature as C
import qualified Model 
import qualified System.Random as Random
import qualified Control.Monad.Random as Rand
import qualified RandomUtils

-- data types

data Board = Board { 
    _boardMap :: GM.GameMap,
    _boardPlayer :: C.Creature, 
    _boardCreatures :: [C.Creature],
    _intendedPlayerTransitions :: [C.Transition],
    _boardRandomGen :: Random.StdGen
}
   
makeLenses ''Board

-- creating new boards

fromMapRandomizingStart :: Random.RandomGen g => GM.GameMap -> C.Creature -> Rand.Rand g Board 
fromMapRandomizingStart gameMap player = do
    position <- RandomUtils.choice (GM.navigableTiles gameMap) 
    let positionedPlayer = player & C.creaturePosition .~ position
    seed <- Rand.getRandom
    return $ Board {
        _boardMap = gameMap, 
        _boardPlayer = positionedPlayer, 
        _boardCreatures = [],
        _intendedPlayerTransitions = [],
        _boardRandomGen = Rand.mkStdGen seed
    }

addCreaturesRandomly :: Random.RandomGen g => [C.Creature] -> Board -> Rand.Rand g Board 
addCreaturesRandomly creatures b = do
    let candidatePositions = filter (\p -> not (any (p ==) (b^..boardCreatures.traverse.C.creaturePosition))) $
                               filter (/= (b^.boardPlayer.C.creaturePosition)) $
                               GM.navigableTiles (b^.boardMap)
    positions <- RandomUtils.sample candidatePositions (length creatures)
    let positionedCreatures = fmap (\(p, c) -> c &C.creaturePosition .~ p) $ zip positions creatures
    return $ b & boardCreatures %~ (positionedCreatures ++)

addCreatureRemovingTiles :: C.Creature -> Board -> Board
addCreatureRemovingTiles c b = b &  boardCreatures %~ (c:) & boardMap . GM.tiles %~ (flip (foldl (flip M.delete)) (C.occupiedTiles c))

-- rendering boards

toCounters :: Board -> [Model.Counter]
toCounters (b@Board{_intendedPlayerTransitions=t:_}) = 
                (C.transitionToCounter t player) : 
                ((uncurry C.transitionToCounter) <$> (nextTransitions b)) ++  
                (Model.gameMapToCounters gameMap)
  where
    player = b^.boardPlayer
    gameMap = b^.boardMap
toCounters (b@Board{_intendedPlayerTransitions=[]}) =
                (C.toCounter player) : 
                (C.toCounter <$> creatures) ++  
                (Model.gameMapToCounters gameMap)
  where
    player = b^.boardPlayer
    creatures = b^.boardCreatures
    gameMap = b^.boardMap

-- game logic

applyTransition :: Board -> C.Transition -> C.Creature -> (Maybe C.Creature, Board)
applyTransition b (C.MoveTransition dst) _ 
    | (Just GM.Hole) ==  (b^.boardMap.GM.tiles.(at dst))  = (Nothing, b & boardMap.GM.tiles.(ix dst) .~ GM.FullHole)
applyTransition b (C.MoveTransition dst) c = (Just $ c & 
            C.creaturePosition .~ dst &
            C.creatureDirection .~ dir , 
            b)
  where 
    dir = GM.angleBetween (c^.C.creaturePosition) dst
applyTransition b (C.PushTransition dst) c = (Just $ c & 
            C.creaturePosition .~ dst &
            C.creatureDirection .~ dir ,
            b)
  where 
    dir = GM.angleBetween (c^.C.creaturePosition) dst
applyTransition b C.NullTransition c = (Just $ c, b)
applyTransition b (C.ExitTransition _ _) c = (Just $ c, b)

data Route = NoRoute | PathRoute [(Int, Int)] | PushRoute [(Int, Int)] | NullRoute (Int, Int) | ExitRoute C.Exit [(Int, Int)]

routeTo :: Board -> (Int, Int) -> Route
routeTo b d = case directRoute of
    Nothing -> case isPushable of
      True -> case reachablePushablePositions of
        [] -> NoRoute
        rpp -> PushRoute $ (minimumBy (compare `on` length) rpp) ++ [d]
      False -> case exit of 
        Just e -> case reachableNeighbours of
          [] -> NoRoute
          rn -> ExitRoute e $ (minimumBy (compare `on` length) rn) ++ [d]
        Nothing -> NoRoute
    Just [] -> NullRoute d
    Just r -> PathRoute r
  where 
    pathTo t = GM.routeBetween (b^.boardMap) (concatMap C.occupiedTiles (b^.boardCreatures)) (b^.boardPlayer.C.creaturePosition) t
    directRoute = pathTo d
    creaturesOnTile t = filter (\c -> any (== t) $ C.occupiedTiles c) (b^.boardCreatures)
    isPushable = any C.isPushable (creaturesOnTile d)
    exit = (join . find isJust) $ C.getExit <$> (creaturesOnTile d)
    pushablePositions = filter (null.creaturesOnTile.fst) $ 
        filter (null.creaturesOnTile.snd) $
            GM.pushableNeighbours (b^.boardMap) d
    reachablePushablePositions = catMaybes $ pathTo.fst <$> pushablePositions
    neighbours = filter (null.creaturesOnTile) $ 
            (G.neighbours (b^.boardMap.GM.grid) =<< (C.occupiedTiles =<< (creaturesOnTile d)))
    reachableNeighbours = catMaybes $ pathTo <$> neighbours
nextPlayerPosition :: Board -> (Int, Int)
nextPlayerPosition Board {_intendedPlayerTransitions=(C.MoveTransition d):_} = d
nextPlayerPosition Board {_intendedPlayerTransitions=(C.PushTransition d):_} = d
nextPlayerPosition b = b^.boardPlayer.C.creaturePosition

nextTransition :: Board -> [C.Creature] -> C.Creature -> Rand.Rand Random.StdGen C.Transition
nextTransition b others (c@C.Creature{C._species = (C.Monster _ C.RandomWalk)}) = 
  case candidatePositions of
    [] -> return $ C.NullTransition
    ds -> C.MoveTransition <$> RandomUtils.choice ds
  where
    candidatePositions = filter (/= nextPlayerPosition b) $ 
      filter (\p -> all ((p /=) . (^.C.creaturePosition)) others) $
        GM.moveableNeighbours (b^.boardMap) (c^.C.creaturePosition)
nextTransition (b@Board{_intendedPlayerTransitions=(C.PushTransition p):_})
               _ 
               (c@C.Creature{C._species = (C.Monster C.Pushable _)}) 
               | p == (c^.C.creaturePosition) =
    return $ C.MoveTransition (
      GM.nextAfter (b^.boardPlayer.C.creaturePosition) (c^.C.creaturePosition))
nextTransition _ _ (C.Creature{C._species = (C.Monster _ C.NoBehaviour)}) =
   return $ C.NullTransition
nextTransition _ _ (C.Creature{C._species = C.Player}) =
   return $ C.NullTransition

nextTransitions :: Board -> [(C.Transition, C.Creature)]
nextTransitions b = 
    Rand.evalRand (
      foldl (\(resultSoFar) -> (\c -> do
        partialResult <- resultSoFar
        -- we're ignoring changes to the board here, 
        -- other than the direct impact of each transition on each creature
        -- because at the moment the only changes are that happen are
        -- holes being filled in
        -- and creatures not moving onto holes on the turn that they're filled
        -- is a feature, not a bug
        let movedCreatures = catMaybes $ (fst.(uncurry (applyTransition b))) <$> partialResult
        let yetToMove = drop (1 + length partialResult) (b ^. boardCreatures)
        let others = movedCreatures ++ yetToMove
        t <- nextTransition b others c 
        return $ (t, c):partialResult
      ))
      (return [])
      (b ^. boardCreatures)
    ) $ (b^.boardRandomGen)
  where

isInTransition :: Board -> Bool
isInTransition = not . null . (^.intendedPlayerTransitions)

nextTransitionValid :: Board -> Bool
nextTransitionValid (b@Board{_intendedPlayerTransitions=((C.MoveTransition dst):_)})=
     not $ any (== dst) (b^..boardCreatures.traverse.C.creaturePosition)
nextTransitionValid (b@Board{_intendedPlayerTransitions=((C.PushTransition dst):_)})=
    not $ any (== GM.nextAfter (b^.boardPlayer.C.creaturePosition) dst)
         (b^..boardCreatures.traverse.C.creaturePosition)
nextTransitionValid _ = True 

validateNextTransition :: Board -> Board
validateNextTransition b = if nextTransitionValid b then b else b & intendedPlayerTransitions .~ []


advanceCreatureTransitions :: Board -> Board
advanceCreatureTransitions b = nextBoard & boardCreatures .~ nextCreatures
  where
    creatureTransitions = nextTransitions b
    (nextCreatures, nextBoard) = foldl (\(cs, accB) -> (\(t, c) -> 
            case applyTransition accB t c of
                (Just nc, nb) -> (nc:cs, nb)
                (Nothing, nb) -> (cs, nb)
        )) ([], b) creatureTransitions

advance :: Board -> Either C.Exit Board
advance (b@Board {_intendedPlayerTransitions = (t:ts)}) = 
  case t of
    C.ExitTransition e _ -> Left e
    _ -> Right $ b &
      advanceCreatureTransitions &
      intendedPlayerTransitions .~ ts &
      boardPlayer %~ (fromJust . fst . (applyTransition b t)) &
      boardRandomGen %~ (snd . Random.split) & 
      validateNextTransition

advance (b@Board{_intendedPlayerTransitions=[]}) = Right b
 
cancelTransitions :: Board -> Board
cancelTransitions b = b & intendedPlayerTransitions %~ (take 1)
 
moveTo :: Board -> (Int, Int) -> Board 
moveTo b dst = case (routeTo b dst) of
    PathRoute path -> b & 
        intendedPlayerTransitions .~ 
          (C.MoveTransition <$> path) & 
        validateNextTransition
    NullRoute _ -> b & 
        intendedPlayerTransitions .~ 
          [C.NullTransition] & 
        validateNextTransition
    PushRoute path -> b & 
       intendedPlayerTransitions .~ (
         (C.MoveTransition <$> init path) ++ [C.PushTransition $ last path]) &
       validateNextTransition
    ExitRoute e path -> b & 
       intendedPlayerTransitions .~ (
         (C.MoveTransition <$> init path) ++ [C.ExitTransition e dst]) &
       validateNextTransition
    NoRoute -> b
   
