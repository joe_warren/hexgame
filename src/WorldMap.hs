{-# LANGUAGE TemplateHaskell #-}
module WorldMap 
( WorldMap (..)
, worldTree
, fromTree
, moveDownwards
, moveUpwards
, worldCurrentBoard
) where

import qualified Data.Tree.Zipper as Zipper
import qualified Data.Tree as Tree
import qualified Board
import Control.Lens

data WorldMap = WorldMap {
    _worldTree :: Zipper.TreePos Zipper.Full Board.Board
  }

fromTree :: Tree.Tree Board.Board -> WorldMap
fromTree = WorldMap . Zipper.fromTree

makeLenses ''WorldMap

moveDownwards :: Int -> WorldMap -> WorldMap
moveDownwards n t = t & worldCurrentBoard. Board.intendedPlayerTransitions .~ []
                      &  worldTree %~ (\tr -> case Zipper.childAt n tr of
                                                Just newTree -> newTree
                                                Nothing -> tr
                                            )

moveUpwards :: WorldMap -> WorldMap
moveUpwards t = t & worldCurrentBoard. Board.intendedPlayerTransitions .~ []
                  & worldTree %~ (\tr -> case Zipper.parent tr of
                                                Just newTree -> newTree
                                                Nothing -> tr
                                            )

worldCurrentBoard :: Simple Lens WorldMap Board.Board
worldCurrentBoard = lens (Zipper.label . (^.worldTree)) (\wm -> \b -> wm & worldTree %~ (Zipper.setLabel b))
