{-# LANGUAGE TemplateHaskell #-}
module HexGame (doIt) where

import Control.Monad
import Graphics.UI.GLUT
import Prelude hiding ( init )
import Control.Monad.Random (evalRandIO)
import Linear.Matrix ((!*!))
import qualified Linear as L
import Data.IORef
import qualified Data.Set as S
import qualified Data.Tree as Tree
import Control.Lens
import LoadShaders
import qualified Math.Geometry.Grid.HexagonalInternal as HGI
import qualified Creature as C
import qualified GameMap as GM
import qualified RenderModel as RM
import qualified Model
import qualified GLFunctions as GLF
import qualified Trackball as TB
import qualified Cursor as Cur
import qualified WorldMap
import qualified Board 

data Descriptor = Descriptor {
   _descriptorRenderContext :: RM.RenderContext,
   _descriptorInitialTransitionTime :: Float,
   _descriptorWorldMap :: WorldMap.WorldMap,
   _descriptorCursor :: Cur.Cursor,
   _descriptorTrackball :: TB.Trackball,
   _descriptorKeys :: S.Set Key
}

makeLenses ''Descriptor

descriptorBoard :: Simple Lens Descriptor Board.Board
descriptorBoard = descriptorWorldMap . WorldMap.worldCurrentBoard

doIt :: IO ()
doIt = main

isInTransition :: Descriptor -> Bool
isInTransition = Board.isInTransition . (^. descriptorWorldMap . WorldMap.worldCurrentBoard)

transitionAnimTime :: Float
transitionAnimTime = 1000

init :: IO Descriptor
init = do
  program <- loadShaders [
     ShaderInfo VertexShader (FileSource "triangles.vert"),
     ShaderInfo FragmentShader (FileSource "triangles.frag")]
  currentProgram $= Just program

  depthFunc $= Just Less

  activeTexture            $= TextureUnit 0

  location0 <- get (uniformLocation program "tex")
  uniform location0 $= (TextureUnit 0)

  modelFn <- RM.initModelDescriptors
  let wizard = C.Creature C.Player (0, 0) 3 [[]] Model.PlayerModel
  let spider = C.Creature (C.Monster C.NoAction C.RandomWalk) (0, 0) 3 [[]] Model.SpiderModel
  let barrel = C.Creature (C.Monster C.Pushable C.NoBehaviour) (0, 0) 0 [[]] Model.BarrelModel
  let staircaseDown = C.Creature (C.Monster (C.ExitAction (C.ExitDownwards 0))C.NoBehaviour) (3, 0) 0 [[], [HGI.Northwest], [HGI.Northeast]] Model.StaircaseDownModel
  let staircaseUp = C.Creature (C.Monster (C.ExitAction (C.ExitUpwards)) C.NoBehaviour) (-3, 0) 0 [[], [HGI.Northwest], [HGI.Northeast]] Model.StaircaseUpModel

  boardA <- evalRandIO $ do 
                  gameMap <- GM.newMap 5 15
                  (Board.fromMapRandomizingStart gameMap wizard) >>=
                    (return . (Board.addCreatureRemovingTiles staircaseDown)) >>=
                    (Board.addCreaturesRandomly (replicate 3 spider)) >>=
                    (Board.addCreaturesRandomly (replicate 3 barrel)) 
  boardB <- evalRandIO $ do 
                  gameMap <- GM.newMap 5 15
                  (Board.fromMapRandomizingStart gameMap wizard) >>=
                    (return . (Board.addCreatureRemovingTiles staircaseDown)) >>=
                    (return . (Board.addCreatureRemovingTiles staircaseUp)) >>=
                    (Board.addCreaturesRandomly (replicate 3 spider)) >>=
                    (Board.addCreaturesRandomly (replicate 3 barrel)) 

  boardC <- evalRandIO $ do 
                  gameMap <- GM.newMap 7 20
                  (Board.fromMapRandomizingStart gameMap wizard) >>=
                    (return . (Board.addCreatureRemovingTiles staircaseUp)) >>=
                    (Board.addCreaturesRandomly (replicate 3 spider)) >>=
                    (Board.addCreaturesRandomly (replicate 3 barrel)) 


  let worldMap = WorldMap.fromTree $ Tree.Node boardA [Tree.Node boardB [Tree.Node boardC []]]

  let curs = Cur.Cursor (Just (1, 1))
  et <- get elapsedTime
  let time =  ((fromIntegral et)) :: Float

  vLocation <- get (attribLocation program "position")
  uvLocation <- get (attribLocation program "texCoord")
  mvLocation <- get (uniformLocation program "modelview")

  let programLocs = GLF.ProgramLocations mvLocation vLocation uvLocation
  let renderContext = RM.RenderContext programLocs modelFn

  return $ Descriptor {
    _descriptorRenderContext = renderContext,
    _descriptorInitialTransitionTime = time,
    _descriptorWorldMap = worldMap, 
    _descriptorCursor = curs,
    _descriptorTrackball = TB.newTrackball,
    _descriptorKeys = S.empty
  }

descriptorViewMatrix :: Descriptor -> L.M44 Float
descriptorViewMatrix = TB.trackballMatrix . (^.descriptorTrackball)

descriptorMatrix :: Descriptor -> IO (L.M44 Float)
descriptorMatrix d = do
    projection <- GLF.projectionMatrix
    return $ projection !*! (descriptorViewMatrix d)

shouldDisplayCursor :: Descriptor -> Bool
shouldDisplayCursor desc = not $ (isInTransition desc) || ((keyPressed $ SpecialKey KeyCtrlL) desc)

display :: Descriptor -> DisplayCallback
display desc@(Descriptor renderContext initialTransitionTime worldmap curs _ _) = do
  clear [ ColorBuffer, DepthBuffer ]

  et <- get elapsedTime
  let board = worldmap ^. WorldMap.worldCurrentBoard
  let time =  ((fromIntegral et)) :: Float
  let fractionalTime = ((time - initialTransitionTime)/transitionAnimTime)
  let partialTime = if isInTransition desc then fractionalTime else 0
  mat <- descriptorMatrix desc
  void $ sequence $ (RM.renderCounter renderContext mat partialTime <$> (Board.toCounters board))
  if shouldDisplayCursor desc
     then void . sequence $ 
        (RM.renderCounter renderContext mat time <$> 
            (Cur.toCounters curs (desc^.descriptorWorldMap . WorldMap.worldCurrentBoard)))
     else return ()
  swapBuffers

transformCursor :: IORef Descriptor -> (Cur.Cursor -> Cur.Cursor) -> IO ()
transformCursor ref fn = 
  modifyIORef ref (descriptorCursor %~ fn)

transformTrackball :: IORef Descriptor -> (TB.Trackball -> TB.Trackball) -> IO ()
transformTrackball ref fn = 
  modifyIORef ref (descriptorTrackball%~ fn)

keyPressed :: Key -> Descriptor -> Bool
keyPressed k = (S.member k). (^.descriptorKeys)

onKey :: IORef Descriptor -> Key -> KeyState -> p1 -> p2 -> IO ()
onKey ref key Down _ _ = do
    transformKeys ref (S.insert key)
onKey ref key Up _ _ = transformKeys ref (S.delete key)

moveTo :: Descriptor -> Descriptor
moveTo d = case endPos of
             Just p -> d & descriptorWorldMap . WorldMap.worldCurrentBoard %~ ((flip Board.moveTo) p)
             Nothing -> d
  where 
    endPos = Cur.position $ (d^.descriptorCursor)

setTransitionStart :: IORef Descriptor -> IO ()
setTransitionStart ref = do
    et <- fromIntegral <$> get elapsedTime
    modifyIORef ref (descriptorInitialTransitionTime .~ et)

onMouse :: IORef Descriptor -> MouseButton -> KeyState -> p -> IO ()
onMouse ref WheelUp _ _ = transformTrackball ref TB.scrollUp
onMouse ref WheelDown _ _ = transformTrackball ref TB.scrollDown
onMouse ref LeftButton Down _ = do
    ctrlDown <- (keyPressed $ SpecialKey KeyCtrlL) <$> get ref
    if ctrlDown then
        transformTrackball ref TB.click
      else do
        stationary <- isInTransition <$> get ref
        if not stationary then do
            setTransitionStart ref
            modifyIORef ref moveTo
          else return ()
onMouse ref LeftButton Up _ = do
    transformTrackball ref TB.unclick
onMouse _ _ _ _ = return ()

onMouseMove :: IORef Descriptor -> Position -> IO ()
onMouseMove ref (Position x y) = do 
    p <- GLF.backProject (fromIntegral x) (fromIntegral y)
    mat <- (L.inv44 . (descriptorViewMatrix)) <$> get ref
    let worldPos = (GLF.normalizeV4 . (mat L.!*)) <$> p
    transformCursor ref (Cur.move $ worldPos)
    case worldPos of 
      Nothing -> return ()
      Just c -> do
        shiftDown <- (keyPressed $ SpecialKey KeyShiftL) <$> get ref
        transformTrackball ref (TB.drag shiftDown c)

transformKeys :: IORef Descriptor -> (S.Set Key -> S.Set Key) -> IO ()
transformKeys ref fn = 
  modifyIORef ref (descriptorKeys %~ fn)

updateTransitions :: Float -> Descriptor -> Descriptor
updateTransitions _ d | False == isInTransition d = d
updateTransitions time d = d & descriptorInitialTransitionTime .~ nextTransitionTime & descriptorWorldMap .~ nextWorldMap
  where
    initTime = d^.descriptorInitialTransitionTime
    timeElapsed = time - initTime
    moveOn = timeElapsed > transitionAnimTime
    cancelTransitions = keyPressed (Char '\27') d
    thisWorldMap = d^.descriptorWorldMap
    thisBoard = d^.descriptorBoard
    advance = case Board.advance thisBoard of
        Right nextBoard -> WorldMap.worldCurrentBoard .~ nextBoard
        Left (C.ExitDownwards n) -> WorldMap.moveDownwards n 
        Left C.ExitUpwards -> WorldMap.moveUpwards 
    nextWorldMap = (if moveOn then advance else id) .
                (if cancelTransitions then (WorldMap.worldCurrentBoard%~Board.cancelTransitions) else id) $
                thisWorldMap
    nextTransitionTime = if moveOn then initTime + transitionAnimTime else initTime

timestep :: IORef Descriptor -> IO ()
timestep ref = do
    readIORef ref >>= display
    time <- fromIntegral <$> get elapsedTime 
    modifyIORef ref (updateTransitions time)
main :: IO ()
main = do
  (progName, _args) <- getArgsAndInitialize
  initialDisplayMode $= [ RGBAMode, DoubleBuffered, WithDepthBuffer, WithSamplesPerPixel 4 ]
  initialWindowSize $= Size 1024 1024
  initialContextVersion $= (3, 0)
  initialContextProfile $= [ CoreProfile ]
  _ <- createWindow progName
  descriptor <- init
  ref <- newIORef descriptor
  motionCallback $= Just (onMouseMove ref)
  passiveMotionCallback $= Just (onMouseMove ref)
  -- the order that keyboardMouse and mouseCallback are set really matters :(
  keyboardMouseCallback $= Just (onKey ref)
  mouseCallback $= Just (onMouse ref)
  displayCallback $= timestep ref
  idleCallback $= Just (timestep ref)
  mainLoop
