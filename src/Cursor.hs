module Cursor 
( Cursor (..)
, move
, toCounters
) where

import qualified Linear as L
import qualified GameMap as GM
import qualified Creature as C
import qualified Model
import qualified Board
import qualified Math.Geometry.Grid as G
import qualified Math.Geometry.Grid.Hexagonal as HG
import qualified Math.Geometry.Grid.HexagonalInternal as HGI

import Control.Lens

data Cursor = Cursor {
  position :: Maybe (Int, Int)
}

move :: Maybe (L.V3 Float) -> Cursor -> Cursor
move Nothing c = c{position=Nothing}
move (Just p) c = c { position = Just $ GM.cartesianToHex (x, y)}
  where
    L.V3 x y _ = p

pathCounters :: Model.CursorState -> Int -> [(Int, Int)] -> [Model.Counter]
pathCounters endCursor angle path = (\(p, m) -> Model.Stationary $ Model.StationaryCounter p angle m) <$>
             zip (reverse path) 
                (Model.CursorModel <$> 
                    (endCursor : (repeat Model.CursorStart)))
toCounters' :: (Int, Int) -> Board.Board -> [Model.Counter]
toCounters' cursorPos b = case Board.routeTo b cursorPos of
     Board.PathRoute path -> pathCounters Model.CursorEnd 0 path
     Board.ExitRoute (C.ExitDownwards _) path -> pathCounters Model.CursorArrowDown 0 path
     Board.ExitRoute C.ExitUpwards path -> pathCounters Model.CursorArrowUp 0 path
     Board.NullRoute pos -> pure $ Model.Stationary $ Model.StationaryCounter pos 0 
                (Model.CursorModel Model.CursorTimer)
     Board.PushRoute path -> pathCounters Model.CursorArrow (lastCounterDirection b path) path
     Board.NoRoute -> []
angleBetween :: (Int, Int) -> (Int, Int) -> Int
angleBetween src dst = 
  case (head $ G.directionTo HG.UnboundedHexGrid src dst) of
    HGI.Southwest -> 0
    HGI.Southeast -> 1
    HGI.East -> 2
    HGI.Northeast -> 3
    HGI.Northwest -> 4
    HGI.West -> 5

lastCounterDirection :: Board.Board -> [(Int, Int)] -> Int
lastCounterDirection _ [] = 0
lastCounterDirection b (l:[]) = angleBetween (b^.Board.boardPlayer.C.creaturePosition) l
lastCounterDirection _ (sl:l:[]) = angleBetween sl l
lastCounterDirection b (_:xs) = lastCounterDirection b xs


toCounters :: Cursor -> Board.Board -> [Model.Counter]
toCounters c b = case (position c) of
    Just p -> toCounters' p b
    Nothing -> []


