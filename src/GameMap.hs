{-# LANGUAGE TemplateHaskell #-}
module GameMap 
( newMap
, GameMap (..)
, grid
, tiles
, Tile (..)
, allTiles
, nextAfter
, routeBetween
, cartesianFromHex
, cartesianToHex
, moveableNeighbours 
, pushableNeighbours
, navigableTiles
, angleBetween
, constructShape
) where

import qualified MinSpanTree as MST
import qualified Math.Geometry.Grid as G
import qualified Math.Geometry.Grid.Hexagonal as HG
import qualified Math.Geometry.Grid.HexagonalInternal as HGI
import qualified Data.Map as M
-- import qualified Data.Graph as GR
import qualified Data.Graph.Inductive.Graph as GR
import qualified Data.Graph.Inductive.PatriciaTree as GRPT
import qualified Data.Graph.Inductive.Query.SP as GRSP
import qualified Data.Bits as Bits

import RandomUtils
import Control.Lens
import System.Random
import Control.Monad.Random
import Data.Monoid()
import Data.Maybe

data Tile = Grass | Path | Water | Hole | FullHole deriving (Eq, Ord, Enum, Bounded, Show)

allTiles :: [Tile]
allTiles = [minBound ..]

data GameMap = GameMap {
    _grid :: HG.HexHexGrid,
    _tiles :: M.Map (Int, Int) Tile
}
makeLenses ''GameMap

emptyMap ::Int -> Tile -> GameMap
emptyMap size tile = GameMap theGrid plainMap
  where
    theGrid = HG.hexHexGrid size
    plainMap = M.fromList $ (\k -> (k, tile)) <$> G.indices theGrid

setRandomTiles :: RandomGen g => Float -> Tile -> GameMap -> Rand g GameMap
setRandomTiles frac t gameMap = do
    let count = G.tileCount $ gameMap^.grid
    inds <- sample (G.indices $ gameMap^.grid) (floor $ (fromIntegral count) * frac)
    let mapUpdates = M.fromList $ (\k -> (k, t)) <$> inds
    return $ gameMap & tiles%~(mapUpdates<>)

overlayRoadNetwork :: RandomGen g => Int -> Tile -> GameMap -> Rand g GameMap
overlayRoadNetwork lociCount t gm = do
    lociIx <- sample (G.indices $ gm^.grid) lociCount
    let edges = (do
                    src <- lociIx
                    dst <- lociIx
                    return $ MST.Edge src dst (fromIntegral $ G.distance (gm^.grid) src dst)
                )
    let mst = MST.minSpanTree edges
    lociPaths <- fmap concat $ sequence $ (\(MST.Edge src dst _) -> choice $ G.minimalPaths (gm^.grid) src dst) <$> mst
    let roadMap =  M.fromList $ (\k -> (k, t)) <$> lociPaths
    return $ gm & tiles %~ (roadMap <>)

newMap :: RandomGen g => Int -> Int -> Rand g GameMap
newMap size lociCount = (return $ emptyMap size Grass) >>=
                   (setRandomTiles 0.5 Water) >>=
                   (overlayRoadNetwork lociCount Path) >>=
                   (setRandomTiles 0.05 Hole)
cartesianFromHex :: (Int, Int) -> (Float, Float)
cartesianFromHex (x, y) = (xc, yc)
  where 
    x1 = fromIntegral x
    y1 = fromIntegral y
    xc = 1.5*x1
    yc = (2*y1 + x1) * cos (pi/6)

cartesianToHex :: (Float, Float) -> (Int, Int)
cartesianToHex (xc, yc) = (x, y)
  where
    x1 = xc/1.5
    y1 = ((yc/(cos (pi/6))) - x1)/2
    x = round x1
    y = round y1

navigable :: Tile -> Bool
navigable Water = False
navigable Hole = False
navigable _  = True

navigableTiles :: GameMap -> [(Int, Int)]
navigableTiles m = filter indexNavigable $ G.indices $ m^.grid
  where 
    tile = m^.tiles
    indexNavigable i = fromMaybe False $ navigable <$> (tile M.!? i)

passable :: Tile -> Tile -> Bool
passable Water _ = False
passable _ Water = False
passable Hole _ = False
passable _ Hole = False
passable _ _ = True

pushable :: Tile -> Tile -> Bool
pushable s Hole | navigable s = True
pushable s d = passable s d

movementCost :: Tile -> Tile -> Float
movementCost Path Path = 0.8
movementCost Path _ = 0.9
movementCost _ Path = 0.9
movementCost _ _ = 1.0

unflatternIndex :: Int -> (Int, Int)
unflatternIndex x = (fromIntegral (a -off), fromIntegral (b - off))
  where ind = toInteger x
        mask = 2 ^ ss - 1
        ss = 16
        off = (2::Integer) ^ (15::Integer) - 1
        a = ((ind `Bits.shiftR` ss) Bits..&. mask)
        b = (ind Bits..&. mask)

flatternIndex :: (Int, Int) -> Int
flatternIndex (x, y) = fromIntegral ((indexX `Bits.shiftL` ss) Bits..|. indexY)
  where indexX = off + toInteger x
        indexY = off + toInteger y
        ss = 16
        off = (2::Integer) ^ (15::Integer) - 1 

routeBetween :: GameMap -> [(Int, Int)] -> (Int, Int) -> (Int, Int) -> Maybe [(Int, Int)]
routeBetween m badLocs src dst = possiblePath
  where 
    forbidden i = any (==i) badLocs
    tile = m^.tiles
    indexPassable' s d = fromMaybe False $ passable <$> (tile M.!? s) <*> (tile M.!? d)
    indexPassable s d = indexPassable' s d && (not $ forbidden d)
    indexCost s d = fromMaybe 1.0 $ movementCost <$> (tile M.!? s) <*> (tile M.!? d)
    nodes = (\i -> (flatternIndex i, i)) <$> G.indices (m^.grid)
    edges = fmap (\(s, d) -> (flatternIndex s, flatternIndex d, indexCost s d)) $
        filter (uncurry indexPassable) $
        concatMap (\(s, d) -> [(s, d), (d, s)]) $
        G.edges (m^.grid)
    graph :: GRPT.Gr (Int, Int) Float
    graph = GR.mkGraph nodes edges
    sp = GRSP.sp (flatternIndex src) (flatternIndex dst) graph
    possiblePath = (fmap $ tail . (fmap unflatternIndex))  sp

moveableNeighbours :: GameMap -> (Int, Int) -> [(Int, Int)]
moveableNeighbours m t = filter indexPassable neighbours
  where 
    neighbours = G.neighbours (m^.grid) t
    tile = m^.tiles
    indexPassable d = fromMaybe False $ passable <$>  (tile M.!? t) <*> (tile M.!? d)
 
nextAfter :: (Int, Int) -> (Int, Int) -> (Int, Int)
nextAfter a b = case G.neighbour gr b (head $ G.directionTo gr a b) of 
    Just c -> c
    Nothing -> error "It shouldn't be possible to move outside an infinite grid"
  where 
    gr = HG.UnboundedHexGrid

pushableNeighbours :: GameMap -> (Int, Int) -> [((Int, Int), (Int, Int))]
pushableNeighbours m t = pushables
  where
    neighbours = G.neighbours (m^.grid) t
    opposite s = G.neighbour (m^.grid) t (head $ G.directionTo (m^.grid) s t)
    tile = m^.tiles
    indexPushable s d = fromMaybe False $ pushable <$>  (tile M.!? s) <*> (tile M.!? d)
    potentialPushables = catMaybes $ fmap (\i -> ((,) i) <$> opposite i) neighbours
    pushables = filter ((indexPushable t) . snd) $
                    filter ((indexPushable t) . fst) $ potentialPushables

angleBetween :: (Int, Int) -> (Int, Int) -> Int
angleBetween src dst = 
  case (head $ G.directionTo HG.UnboundedHexGrid src dst) of
    HGI.Southwest -> 0
    HGI.Southeast -> 1
    HGI.East -> 2
    HGI.Northeast -> 3
    HGI.Northwest -> 4
    HGI.West -> 5

followDirections :: (Int, Int) -> [HGI.HexDirection] -> (Int, Int)
followDirections pos directions = foldl ((fromJust .).(G.neighbour g)) pos directions 
  where g = HG.UnboundedHexGrid

constructShape :: [[HGI.HexDirection]] -> (Int, Int) -> [(Int, Int)]
constructShape directions start = (followDirections start) <$> directions
    
