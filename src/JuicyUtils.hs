module JuicyUtils (loadImage) where 

import Codec.Picture

import Data.Vector.Storable (unsafeWith)

--import Graphics.Rendering.OpenGL.GL.Texturing.Specification (texImage2D, Level, Border, TextureSize2D(..), TextureTarget2D (..))
--import Graphics.Rendering.OpenGL.GL.PixelRectangles.ColorTable (Proxy(..), PixelInternalFormat(..))
--import Graphics.Rendering.OpenGL.GL.PixelRectangles.Rasterization (PixelData(..), PixelFormat(RGBA))
--import Graphics.Rendering.OpenGL.GL.DataType (UnsignedByte)

import Graphics.Rendering.OpenGL

import GHC.Int
import GHC.Word

getRaw :: (Image PixelRGBA8) -> IO (PixelData Word8)
getRaw (Image _ _ dat) =
  -- Access the data vector pointer
  unsafeWith dat $ \ptr ->
      -- The pixel data: the vector contains Bytes, in RGBA order
      return (PixelData RGBA UnsignedByte ptr)

getSize :: Image PixelRGBA8 -> (Int32, Int32)
getSize (Image width height _) = (fromIntegral width, fromIntegral height)

loadImage :: String -> IO TextureObject
loadImage filename = do
    Right image <- readImage filename
    -- Generate 1 texture object
    [texObject] <- genObjectNames 1

    -- Make it the "currently bound 2D texture"
    textureBinding Texture2D $= Just texObject

    let converted = convertRGBA8 image
    raw <- getRaw $ converted   
    let (width, height) = getSize converted
    texImage2D
      -- No cube map
      Texture2D
      -- No proxy
      NoProxy
      -- No mipmaps
      0
      -- Internal storage format: use R8G8B8A8 as internal storage
      RGBA8
      -- Size of the image
      (TextureSize2D width height)
      -- No borders
      0
      raw
    return texObject

