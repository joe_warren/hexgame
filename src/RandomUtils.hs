module RandomUtils 
( sample
, choice
) where

import System.Random
import Control.Monad.Random

sample :: RandomGen g => [a] -> Int -> Rand g [a]
sample _ 0 = return []
sample items n = do 
    index <- getRandomR (0, (length items)-1)
    let item = items !! index
    
    let removed = (take index items) ++ (drop (index+1) items)
    remainder <- sample removed (n-1)
    return $ item : remainder

choice :: RandomGen g => [a] -> Rand g a
choice items = ((!!) items) <$> getRandomR (0, (length items)-1)
