#version 120
attribute vec3 position;
attribute vec2 texCoord;

uniform mat4 modelview;

varying vec3 pos;
varying vec2 uv;
void
main()
{
   uv = texCoord; 
   gl_Position = modelview * vec4(position, 1.0);
   pos = position;
}
