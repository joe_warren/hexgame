#version 120

varying vec2 uv;
uniform sampler2D tex;



void main()
{
   //fColor = vec4(1, 1, 0, 0);  
   gl_FragColor = vec4(texture2D(tex, uv.xy).rgb, 1.0);
   //gl_FragColor = vec4(uv.x, uv.y, 0, 0);  
}
