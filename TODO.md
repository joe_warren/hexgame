# TODO.md

* [] Lens up all the records
  * [x] lens up the Board module
  * [x] lens up the Creature module
  * [] prism up the Creature module
  * [x] lens up the GameMap module
* [x] `-Wall` and then fix all the errors 😱
* [] improve viewport
  * [x] Pan (shift + ctrl)
  * [] Zoom should also tilt
  * [] Use `Z=0` plane for clicks, rather than unprojecting?
* [] More player animations than just walking:
  * [x] Framework
    * [] Pushing
    * [] Skip Turn
  * [] Models
    * [] Crafting
    * [] Melee
    * [] Spellcasting
    * [] Pushing
* [] Additional playable characters
* [x] Non Player creatures: 
  * [x] Framework
  * [] Models
    * [x] Spiders
    * [] Puppies
    * [] Gingerbread men
    * [] Knights
    * [] Dragons
* [] Combat
* [] Hexoban
 * [x] Barrel Model
 * [x] Pushable Objects
 * [x] Holes
 * [] Design some Hexoban maps
* [] Inventory
* [] Crafting
* [] More than one level/Stage
  * [] More than one level "type"
  * [x] Combinator up the "Board"
  * [] Global "World" Model (Tree or Graph, Probably Tree)
* [] More Tile Types:
  * [] Dirt/Underground
  * [] Sand
* [x] Impassible/Unpickable objects
  * [] Trees
  * [] Walls
  * [] Buildings
  * [] "gates" between levels
    * [] Staircases 
        * [] Up
        * [] Down
    * [] Signposts
    * [] Doors
* [] Passable/Pickable Objects
  * [] Corpses/Drops
* [] Game state should be readable/showable to disk
