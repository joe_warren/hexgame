def vec3Array(v):
    return [v.x, v.y, v.z]

def vec2Array(v):
    return [v.x, v.y]

def deQuaternion(mat, quat):
    q = Quaternion(quat)
    m = q.to_matrix()
    debone = Matrix([[1,0,0],[0,0,-1], [0,1,0]])
    mat3 = mat.to_quaternion().to_matrix()
    r = mat3 * (m) * (mat3.inverted()) 
    rq = r.to_quaternion()
    return [rq.w, rq.x, rq.y, rq.z]

def traverseArmature(bone):
    children = [traverseArmature(b) for b in bone.children]
    allCurves = bpy.data.actions['ArmatureAction'].fcurves
    #_, bone_q, _ = (bone.matrix * debone).decompose()
    #print(bone_q[0])
    rest_matrix = bpy.data.objects['Armature'].data.bones[bone.name].matrix_local
    curves = [c for c in allCurves if bone.name == c.group.name]
    frames = [[(c.array_index, vec2Array(p.co)) for p in c.keyframe_points] for c in curves]
    groupedFrames = [{"t": f[0][1][0], "d": deQuaternion(rest_matrix, [v[1][1] for i in range(0, 4) for v in f if v[0] == i]) } for f in zip(*frames)]
    return {
      "name": bone.name,
      "head": vec3Array(bone.head),
      "tail": vec3Array(bone.tail),
      "children": children, 
      "frames": groupedFrames
    }

def traverseFCurves():
    curves = bpy.data.actions['ArmatureAction'].fcurves
    def traverseCurve(c):
        return {
            "name": c.data_path,
            "points": [vec2Array(p.co) for p in c.keyframe_points]
        }
    return [traverseCurve(c) for c in curves]

#print({ "armature": traverseArmature(bpy.data.armatures[0].bones[0]), 
#        "curves": traverseFCurves() })
import json
with open("/home/joseph/spider.json", "w") as f:
  f.write(json.dumps(traverseArmature(bpy.data.objects['Armature'].pose.bones[0])))
